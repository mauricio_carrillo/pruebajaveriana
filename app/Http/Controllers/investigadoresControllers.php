<?php

namespace App\Http\Controllers;

use App\faculta;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\investigadores;
use Illuminate\Support\Facades\Input;

class investigadoresControllers extends Controller
{
    //
    public function store()
    {
        $dataInvestigacion = investigadores::all();
        $facultad = faculta::lists('nombre','id');
        return view('investigadores',['dataInvestigacion'=>$dataInvestigacion,'facultad'=>$facultad]);
    }

    public function crear()
    {
        $dataInvestigacion = new investigadores();
        $dataInvestigacion->nombre =  Input::get('nombre');
        $dataInvestigacion->fecha_nacimiento =  Input::get('fecha_nacimiento');
        $dataInvestigacion->cod_empleado =  Input::get('cod_empleado');
        $dataInvestigacion->faculta =  Input::get('faculta');
        $dataInvestigacion->direccion =  Input::get('direccion');
        $dataInvestigacion->telefono =  Input::get('telefono');
        //$dataInvestigacion->Faculta()->save(1);
        $dataInvestigacion->save();
        return response()->json(['resultado'=>'true']);


    }
    public function delete($id){

        $dataInvestigacion = investigadores::find($id);
        $dataInvestigacion->delete();
        return Redirect()->back();
    }
}
