<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class faculta extends Model
{
    //
    protected $table = 'faculta';
    protected $fillable = ['id','nombre'];
}
