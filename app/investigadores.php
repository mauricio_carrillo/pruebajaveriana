<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class investigadores extends Model
{
    //
    protected $table = 'investigadores';
    protected $fillable = ['id','nombre', 'fecha_nacimiento','cod_empleado','faculta','direccion','telefono'];

    public function faculta(){
        return $this->hasMany('App\Faculta', 'faculta','id');
    }

    public  function relacion(){
        $Resultado = \DB::table('investigadores')
            ->join('investigadores', 'investigadores.faculta', '=', 'facultad.id')
            ->get();
    }


}
