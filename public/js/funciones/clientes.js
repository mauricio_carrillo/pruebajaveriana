/**
/**
 * Created by johanm on 3/02/2016.
 */
$(function () {
    $('#dataFecha .input-daterange').datepicker({
        format: 'DD/MM/YYYY',
        keyboardNavigation: true,
        forceParse: true,
        autoclose: true
    });

});

$(document).ready(function() {
    //crear invetigacion
    var form = $("#crear_investigacion").show();
    $('.input-daterange input').each(function() {
        $(this).datepicker("clearDates");
    });
    $('.input input').each(function() {
        $(this).input("");
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        labels: {
            current: "Paso actual:",
            pagination: "Paginación",
            finish: "Finalizar",
            next: "Siguiente",
            previous: "Anterior",
            loading: "Cargando ..."
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            // Permitir siempre yendo hacia atrás, incluso si el paso actual contiene campos no válidos!
            if (currentIndex > newIndex) {
                return true;
            }

            // Prohibir la supresión de "Advertencia" paso si el usuario es joven
            if (newIndex === 3 && Number($("#age").val()) < 18) {
                return false;
            }

            var form = $(this);

            // Limpie si el usuario se fue hacia atrás antes
            if (currentIndex < newIndex) {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Deshabilitar la validación en campos que son discapacitados o escondido.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Comience la validación; Prevenir el futuro si es falso
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18) {
                $(this).steps("next");
            }

            // Suppress (skip) "Advertencia" paso si el usuario tiene la edad suficiente y quiere el paso anterior.
            if (currentIndex === 2 && priorIndex === 3) {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex) {
            var form = $(this);

            // Deshabilitar la validación en campos que son discapacitados.
            // En este punto se recomienda hacer un control general (significa ignorar campos sólo con discapacidad)
            form.validate().settings.ignore = ":disabled";

            // Comience la validación; Prevenir formulario de envío si es falso
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var form = $(this);
            if (form.valid()) {
                $.ajax({
                    type: "Post",
                    url: "invetigadores/crear",
                    data: form.serialize(), // serializa los elementos del formulario.
                    success: function (data) {
                        console.log(data);
                        if(data.resultado === "true"){
                            alert('Datos guardados');
                                $('#crear_investigacion').modal('hide'),
                                location.reload();

                        }


                    }

                });
            }
            return form.valid();
        },
        onCanceled:function (event, currentIndex) {
                $('#crear_investigacion').modal('hide');
        }
    }).validate({
        errorPlacement: function (error, element) {
            element.before(error);
        },
        rules: {
            nombre: {
                required: true,
                minlength: 5
            },
            fecha_nacimiento: {
                required: true,
            },
            cod_empleado: {
                required: true,
                number: true
            },
            faculta: {
                required: true,
            },
            telefono: {
                number: true
            },
            acceptTerms: {required: true}
        },
        messages: {

            nombre: {
                required: "Requerido",
                minlength: "Minimo 5 Caracteres."
            },
            fecha_nacimiento: {
                required: "Requerido",
            },
            faculta: {
                required: "Requerido",
            },
            cod_empleado: {
                required: "Requerido",
                number: "Solo Números"
            },
            telefono: {
                number: "Solo Números"
            },
            acceptTerms: {
                required: "Requerido",
            },
        },
        labels: {
            cancel: "Cancelar",
            current: "current step:",
            pagination: "Paginación",
            finish: "Finalizar",
            next: "Siguiente",
            previous: "Atras",
            loading: "Loading ..."
        }
    });

});
