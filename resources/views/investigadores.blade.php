<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pruebas | javeriana</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">

            <div class="p-w-md m-t-sm">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">


                            <!--Tablade de Usuarios-->
                            <div class="ibox-content">

                                <div class="row">

                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label" for="product_name">Datos Basicos</label>  -   <a href="#" data-toggle="modal" data-target="#Modal-RegistroInvestigacion"><i class="label label-primary pull-right ">Nuevo-Inv</i></a>
                                        </div>
                                    </div>


                                </div>
                                <!--Modales-->
                                @include('modal.investigadores_modal')

                                <!--Registros tabla Usuarios-->
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>

                                        @foreach($dataInvestigacion as $data)
                                            <tr>

                                                <td>{{ $data->nombre or 'Error' }}</td>

                                                <td>{{ $data->fecha_nacimiento or 'ERROr' }}</td>

                                                <td>{{ $data->cod_empleado or 'Error' }}</td>

                                                <td>{{ $data->faculta or 'Error' }}</td>

                                                <td>{{ $data->telefono or 'Error' }}</td>

                                                <td>{{ $data->direccion or 'Error' }}</td>

                                                <td><a href="{{ route('datadelete', $data->id) }}"><i class="fa fa-check text-navy"></i></a></td>

                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--Modales-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>



<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Jquery Validate -->
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/funciones/clientes.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>

<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.min.js"></script>

<!-- Data picker -->
<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>


<!-- Date range picker -->
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>


<!-- FooTable -->
<script src="js/plugins/footable/footable.all.min.js"></script>


</body>
</html>
