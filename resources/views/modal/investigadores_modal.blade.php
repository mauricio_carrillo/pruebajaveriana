<style>
    .clsDatePicker {
        z-index: 100000;
    }
</style>

<style>
    .wizard > .content > .body { position: inherit; }
    .input-daterange {
        z-index: 100000;
    }
</style>

<div class="modal inmodal" id="Modal-RegistroInvestigacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-plus-square modal-icon"></i>
                <h4 class="modal-title">Nue Registro Invetigadores</h4>
                <small class="font-bold">Por favor completar <strong>todos los campos</strong>.</small>
            </div>
            <div class="ibox-content">
                <form id="crear_investigacion" method="Post" class="wizard"><!--formulario-->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <h3>Registro</h3>
                    <fieldset>

                        <h2>Datos Seguros</h2>
                        <div class="form-group">
                            <label>Nombre*</label>
                            <input id="nombre" name="nombre" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <div class="form-group "  id="dataFecha">
                                <label class="font-noraml">Fecha Nacimiento</label>
                                <div class="input-daterange input-group " data-provide="datepicker" id="datepicker">
                                    <input type="text"id="fecha_nacimiento" name="fecha_nacimiento" readonly="readonly" class="input-sm form-control" placeholder="Fecha Inicial" name="start" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Codigo Empleado</label>
                            <input id="cod_empleado" name="cod_empleado" type="text" class="form-control" placeholder="123" required>
                        </div>
                        <div class="form-group">
                           {!! Form::select('facultad',$facultad, null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>Direccion*</label>
                            <input id="direccion" name="direccion" type="text" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Telefono*</label>
                            <input id="telefono" name="telefono" type="text" class="form-control" >
                        </div>


                    </fieldset>

                    <h3>Advertencia</h3>
                    <fieldset>
                        <h2>Todos los Campos deben ser Registrados</h2>
                        <div class="text-center" style="margin-top: 120px">
                            <p>Terminar Registro  ;-) </p>
                        </div>
                    </fieldset>

                    <h3>Finalizar</h3>
                    <fieldset>
                        <h2>Términos y Condiciones</h2>
                        <p></p>
                        <div class="col-sm-10">
                            <div class="i-checks"><label> <input type="checkbox" id="acceptTerms" name="acceptTerms"> <i></i> Estoy de acuerdo con los términos y condiciones... </label></div>
                        </div>
                    </fieldset>
                </form>
            </div>



        </div>
    </div>
</div>

